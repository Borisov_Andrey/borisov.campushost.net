<?php

use yii\db\Migration;
use yii\db\Schema;

class m161111_074406_addTableNewsSettings extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%news_config}}', [
            'id'                    => Schema::TYPE_PK,
            'name'                  => Schema::TYPE_STRING          . ' NULL COMMENT "Название"',
            'value'                 => Schema::TYPE_STRING          . ' NULL COMMENT "Значение"',
            'created_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время создания записи"',
            'updated_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время редактирование записи"',
        ], $tableOptions);

        //устанавливает время создание записи
        $this->execute("
                CREATE TRIGGER `task_news_config_created_at` BEFORE INSERT ON `news_config` FOR EACH ROW
                BEGIN
                    SET NEW.created_at = NOW();
                END;
        ");

        //устанавливает время обновления записи
        $this->execute("
                CREATE TRIGGER `task_news_config_updated_at` BEFORE UPDATE ON `news_config` FOR EACH ROW
                BEGIN
                    SET NEW.updated_at = NOW();
                END;
        ");

        $this->createIndex('name', 'news_config', 'name', true);
        $this->insert('news_config', ['name' => 'count_record_on_page', 'value' => 20]);
    }

    public function safeDown()
    {
        echo "m161111_074406_addTableNewsSettings cannot be reverted.\n";

        return false;
    }
}
