<?php

use yii\db\Migration;
use yii\db\Schema;

class m161104_143922_addTableNewsSettings extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%news_settings}}', [
            'id'                    => Schema::TYPE_PK,
            'role_id'               => Schema::TYPE_INTEGER         . ' NOT NULL COMMENT "Id роли"',
            'list'                  => Schema::TYPE_BOOLEAN         . ' NULL COMMENT "Право на просмотр списка"',
            'view'                  => Schema::TYPE_BOOLEAN         . ' NULL COMMENT "Право на просмотр отдельной новости"',
            'edit'                  => Schema::TYPE_BOOLEAN         . ' NULL COMMENT "Право на редактирование новости"',
            'add'                   => Schema::TYPE_BOOLEAN         . ' NULL COMMENT "Право на создание новости"',
            'created_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время создания записи"',
            'updated_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время редактирование записи"',
        ], $tableOptions);

        //устанавливает время создание записи
        $this->execute("
                CREATE TRIGGER `task_news_settings_created_at` BEFORE INSERT ON `news_settings` FOR EACH ROW
                BEGIN
                    SET NEW.created_at = NOW();
                END;
        ");

        //устанавливает время обновления записи
        $this->execute("
                CREATE TRIGGER `task_news_settings_updated_at` BEFORE UPDATE ON `news_settings` FOR EACH ROW
                BEGIN
                    SET NEW.updated_at = NOW();
                END;
        ");

        $this->createIndex('role_id', 'news_settings', 'role_id', true);
        $this->addForeignKey('FK_news_settings_role_id', 'news_settings', 'role_id', 'users_roles', 'id');

        $this->insert('news_settings', ['role_id' => 1, 'list' => 1, 'view' => 1, 'edit' => 1, 'add' => 1]);
        $this->insert('news_settings', ['role_id' => 2, 'list' => 1, 'view' => 1, 'edit' => 1, 'add' => 0]);
        $this->insert('news_settings', ['role_id' => 3, 'list' => 1, 'view' => 1, 'edit' => 0, 'add' => 0]);
        $this->insert('news_settings', ['role_id' => 4, 'list' => 1, 'view' => 0, 'edit' => 0, 'add' => 0]);
    }

    public function safeDown()
    {

        echo "m161104_143922_addTableNewsSettings cannot be reverted.\n";

        return false;
    }
}
