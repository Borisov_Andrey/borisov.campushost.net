<?php

use yii\db\Migration;
use yii\db\Schema;

class m161108_100006_addTableEmailTemplates extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%email_templates}}', [
            'id'                    => Schema::TYPE_PK,
            'event'                 => Schema::TYPE_STRING          . ' NULL COMMENT "Имя события"',
            'templates'             => Schema::TYPE_TEXT            . ' NULL COMMENT "Шаблон письма"',
            'created_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время создания записи"',
            'updated_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время редактирование записи"',
        ], $tableOptions);

        //устанавливает время создание записи
        $this->execute("
                CREATE TRIGGER `task_email_templates_created_at` BEFORE INSERT ON `email_templates` FOR EACH ROW
                BEGIN
                    SET NEW.created_at = NOW();
                END;
        ");

        //устанавливает время обновления записи
        $this->execute("
                CREATE TRIGGER `task_email_templates_updated_at` BEFORE UPDATE ON `email_templates` FOR EACH ROW
                BEGIN
                    SET NEW.updated_at = NOW();
                END;
        ");

    }

    public function safeDown()
    {
        echo "m161108_100006_addTableEmailTemplates cannot be reverted.\n";

        return false;    
    }

}
