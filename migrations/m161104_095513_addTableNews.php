<?php

use yii\db\Migration;
use yii\db\Schema;

class m161104_095513_addTableNews extends Migration
{

    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%news}}', [
            'id'                    => Schema::TYPE_PK,
            'name'                  => Schema::TYPE_STRING          . ' NULL COMMENT "Название"',
            'url'                  => Schema::TYPE_STRING           . ' NULL COMMENT "Ссылка"',
            'preview'               => Schema::TYPE_TEXT            . ' NULL COMMENT "Привью"',
            'content'               => Schema::TYPE_TEXT            . ' NULL COMMENT "Контент"',
            'created_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время создания записи"',
            'updated_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время редактирование записи"',
        ], $tableOptions);

        //устанавливает время создание записи
        $this->execute("
                CREATE TRIGGER `task_news_created_at` BEFORE INSERT ON `news` FOR EACH ROW
                BEGIN
                    SET NEW.created_at = NOW();
                END;
        ");

        //устанавливает время обновления записи
        $this->execute("
                CREATE TRIGGER `task_news_updated_at` BEFORE UPDATE ON `news` FOR EACH ROW
                BEGIN
                    SET NEW.updated_at = NOW();
                END;
        ");
    }

    public function safeDown()
    {

        echo "m161104_095513_addTableNews cannot be reverted.\n";

        return false;
    }
}
