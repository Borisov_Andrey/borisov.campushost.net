<?php

use yii\db\Migration;
use yii\db\Schema;

class m161103_105814_addTablesUsers extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users}}', [
            'id'                    => Schema::TYPE_PK,
            'login'                 => Schema::TYPE_STRING          . ' NULL COMMENT ""',
            'password'              => Schema::TYPE_STRING          . ' NULL COMMENT ""',

            'role_id'               => Schema::TYPE_INTEGER         . ' NOT NULL COMMENT ""',
            'account_status_id'     => Schema::TYPE_INTEGER         . ' NOT NULL COMMENT ""',

            'hash'                  => Schema::TYPE_STRING          . ' NULL COMMENT ""',
            'last_active_at'        => Schema::TYPE_DATETIME        . ' NULL COMMENT ""',
            'created_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время создания записи"',
            'updated_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время редактирование записи"',
        ], $tableOptions);

        $this->createIndex('login', 'users', 'login', true);
        $this->createIndex('password', 'users', 'password');

        $this->createIndex('role_id', 'users', 'role_id');
        $this->createIndex('account_status_id', 'users', 'account_status_id');
        $this->createIndex('hash', 'users', 'hash');


        //устанавливает время создание записи
        $this->execute("
                CREATE TRIGGER `task_users_created_at` BEFORE INSERT ON `users` FOR EACH ROW
                BEGIN
                    SET NEW.created_at = NOW();
                END;
        ");

        //устанавливает время обновления записи
        $this->execute("
                CREATE TRIGGER `task_users_updated_at` BEFORE UPDATE ON `users` FOR EACH ROW
                BEGIN
                    SET NEW.updated_at = NOW();
                END;
        ");

        $this->createTable('{{%users_roles}}', [
            'id'                    => Schema::TYPE_PK,
            'name'                  => Schema::TYPE_STRING          . ' NULL COMMENT "Название"',
            'created_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время создания записи"',
            'updated_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время редактирование записи"',
        ], $tableOptions);

        //устанавливает время создание записи
        $this->execute("
                CREATE TRIGGER `task_users_roles_created_at` BEFORE INSERT ON `users_roles` FOR EACH ROW
                BEGIN
                    SET NEW.created_at = NOW();
                END;
        ");

        //устанавливает время обновления записи
        $this->execute("
                CREATE TRIGGER `task_users_roles_updated_at` BEFORE UPDATE ON `users_roles` FOR EACH ROW
                BEGIN
                    SET NEW.updated_at = NOW();
                END;
        ");

        $this->insert('users_roles', ['name' => 'administrators']);
        $this->insert('users_roles', ['name' => 'editors']);
        $this->insert('users_roles', ['name' => 'users']);
        $this->insert('users_roles', ['name' => 'guests']);

        $this->addForeignKey('FK_role_id', 'users', 'role_id', 'users_roles', 'id');

        $this->createTable('{{%account_statuses}}', [
            'id'                    => Schema::TYPE_PK,
            'name'                  => Schema::TYPE_STRING          . ' NULL COMMENT "Название"',
            'created_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время создания записи"',
            'updated_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время редактирование записи"',
        ], $tableOptions);

        //устанавливает время создание записи
        $this->execute("
                CREATE TRIGGER `task_account_statuses_created_at` BEFORE INSERT ON `account_statuses` FOR EACH ROW
                BEGIN
                    SET NEW.created_at = NOW();
                END;
        ");

        //устанавливает время обновления записи
        $this->execute("
                CREATE TRIGGER `task_account_statuses_updated_at` BEFORE UPDATE ON `account_statuses` FOR EACH ROW
                BEGIN
                    SET NEW.updated_at = NOW();
                END;
        ");

        $this->insert('account_statuses', ['name' => 'active']);
        $this->insert('account_statuses', ['name' => 'block']);

        $this->addForeignKey('FK_account_status_id', 'users', 'account_status_id', 'account_statuses', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%account_statuses}}');
        $this->dropTable('{{%users_roles}}');
        $this->dropTable('{{%users}}');
    }
}
