<?php

use yii\db\Migration;
use yii\db\Schema;

class m161105_120610_addTableUsersProfiles extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users_profiles}}', [
            'id'                    => Schema::TYPE_PK,
            'user_id'               => Schema::TYPE_INTEGER         . ' NOT NULL COMMENT "Id пользователя"',
            'first_name'            => Schema::TYPE_STRING          . ' NULL COMMENT "Имя"',
            'last_name'             => Schema::TYPE_STRING          . ' NULL COMMENT "Фамилия"',
            'email_notification'    => Schema::TYPE_BOOLEAN         . ' NULL COMMENT "Уведомление на емайл"',
            'push_notification'     => Schema::TYPE_BOOLEAN         . ' NULL COMMENT "Уведомление пушем"',
            'created_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время создания записи"',
            'updated_at'            => Schema::TYPE_DATETIME        . ' NULL COMMENT "Время редактирование записи"',
        ], $tableOptions);

        //устанавливает время создание записи
        $this->execute("
                CREATE TRIGGER `task_users_profiles_created_at` BEFORE INSERT ON `users_profiles` FOR EACH ROW
                BEGIN
                    SET NEW.created_at = NOW();
                END;
        ");

        //устанавливает время обновления записи
        $this->execute("
                CREATE TRIGGER `task_users_profiles_updated_at` BEFORE UPDATE ON `users_profiles` FOR EACH ROW
                BEGIN
                    SET NEW.updated_at = NOW();
                END;
        ");

        $this->createIndex('user_id', 'users_profiles', 'user_id', true);
        $this->addForeignKey('FK_users_profiles_user_id', 'users_profiles', 'user_id', 'users', 'id');
    }

    public function safeDown()
    {
        echo "m161105_120610_addTableUsersProfiles cannot be reverted.\n";

        return false;
    }

}
