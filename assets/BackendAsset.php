<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BackendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'cp/AdminLTE-2.3.0/bootstrap/css/bootstrap.min.css',
        'cp/AdminLTE-2.3.0/dist/css/AdminLTE.min.css',
        'cp/AdminLTE-2.3.0/dist/css/skins/skin-blue.min.css',
        'cp/AdminLTE-2.3.0/plugins/datatables/dataTables.bootstrap.css',
        'cp/AdminLTE-2.3.0/plugins/iCheck/square/blue.css',
        'cp/styles/common.css'

    ];
    public $js = [
        'cp/AdminLTE-2.3.0/bootstrap/js/bootstrap.min.js',
        'cp/AdminLTE-2.3.0/dist/js/app.min.js',
        'cp/AdminLTE-2.3.0/plugins/datatables/jquery.dataTables.min.js',
        'cp/AdminLTE-2.3.0/plugins/datatables/dataTables.bootstrap.min.js',
        'cp/AdminLTE-2.3.0/plugins/iCheck/icheck.min.js',
        'cp/AdminLTE-2.3.0/plugins/ckeditor/ckeditor.js',
        'cp/js/jquery.function.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];

    public $cssOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}

