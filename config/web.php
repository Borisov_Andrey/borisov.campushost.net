<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',
    'components' => [

        'pusher' => [
            'class'     => 'br0sk\pusher\Pusher',
            //Mandatory parameters
            'appId'     => '265848',
            'appKey'    => '5955a30fac303a448eff',
            'appSecret' => '474f8d07005e4bccc5b2',
            //Optional parameter
            'options'   => ['encrypted' => true]
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '761eb0286b0ba8d813d34598b651c186',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Users',
            'enableAutoLogin' => true,
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        */
        'mailer' => [
            'class' => 'boundstate\mailgun\Mailer',
            'key' => 'key-dac83dd94fa56c8aa01177fe7b9b9ab9',
            'domain' => 'sandbox1aca21d8941049d8a19ba03ddae4e610.mailgun.org',
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],



        'db' => require(__DIR__ . '/db.php'),

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                '/' => 'news/list',
                '/news' => 'news/list',
                '/news/view/<slug>' => 'news/view',

                '<action>'=>'site/<action>',

                'news' => 'news/list',
                '/user/profile' => 'users-profiles/index',

                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

                '<module:\w+>/<controller>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller>/<action:\w+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller>' => '<module>/<controller>/index',
            ],
        ],


        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages', // if advanced application, set @frontend/messages
                    'sourceLanguage' => 'en',
                    'fileMap' => [
                        //'main' => 'main.php',
                    ],
                ],
            ],
        ],

    ],


    'params' => $params,



    'modules' => [
        /*
        'frontend' => [
            'class' => 'app\modules\frontend\frontendModule',
        ],
        */

        'cp' => [
            'class' => 'app\modules\cp\cpModule',

        ],
    ]
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*'] // adjust this to your needs
    ];
}

return $config;
