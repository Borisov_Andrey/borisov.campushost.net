<?php
use yii\helpers\Url;
?>

<?= \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/title.php'); ?>

<div class="row user-default-signup">
    <div class="col-xs-12 col-sm-8 col-md-6 col-md-offset-3 col-sm-offset-2">
        <?=  \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/form_vertical_title.php', ['title' => $this->params['breadcrumbs'][0]['label']]);?>
        <?=  \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/form_vertical.php', ['model' => $model]);?>
        <?php if (\Yii::$app->session->getFlash('profile_saved')) : ?>
            <br>
            <div class="alert alert-success">
                Данные профайла сохранены.
            </div>
        <?php endif ?>
    </div>


</div>


