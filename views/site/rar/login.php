<?php
    use yii\helpers\Url;
?>

<?= \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/title.php'); ?>

<div class="row user-default-signup">
    <div class="col-xs-12 col-sm-8 col-md-6 col-md-offset-3 col-sm-offset-2">

        <?=  \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/form_vertical_title.php', ['title' => $this->params['breadcrumbs'][0]['label']]);?>
        <?=  \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/form_vertical.php', ['model' => $model]);?>
        <?php if ($invalid_login) : ?>
            <br>
            <div class="alert alert-danger" role="alert">
                <strong><?php \Yii::t('Cm', 'Error') ?></strong>
                <?= \Yii::t('Cm', 'Invalid login or(and) password') ?>
            </div>
        <?php endif ?>

        <div class="row">&nbsp;</div>
        <div class="row">
            <div class="col-md-6"><a href="<?= Url::toRoute('/registration') ?>"><?= \Yii::t('Cm', 'Registration') ?></a></div>
            <div class="col-md-6 text-right"><a href="<?= Url::toRoute('/restore-password') ?>"><?= \Yii::t('Cm', 'Restore password') ?></a></div>
        </div>
    </div>
</div>