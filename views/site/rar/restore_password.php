<?php
    use yii\helpers\Url;
    $this->title = '123';
?>

<?= \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/title.php'); ?>

<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-md-offset-3 col-sm-offset-2">
        <?php if (\Yii::$app->session->getFlash('generated_hash')) : ?>
            <br>
            <div class="alert alert-success">
                На ваш емайл было выслано письмо с инструкциями для восстановления пароля, пожалуйста, проверьте свою почту.
            </div>
        <?php else : ?>

            <?=  \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/form_vertical_title.php', ['title' => $this->params['breadcrumbs'][0]['label']]);?>
            <?=  \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/form_vertical.php', ['model' => $model]);?>
            <div class="row">&nbsp;</div>
            <div class="row">&nbsp;</div>
            <div class="row">
                <div class="col-md-6"><a href="<?= Url::toRoute('/login') ?>"><?= \Yii::t('Cm', 'Enter') ?></a></div>
                <div class="col-md-6 text-right"><a href="<?= Url::toRoute('/registration') ?>"><?= \Yii::t('Cm', 'Registration') ?></a></div>
            </div>
        <?php endif ?>
    </div>
</div>