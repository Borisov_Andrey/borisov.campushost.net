users
<?php



echo \yii\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'itemOptions' => ['class' => 'item'],
    'itemView' => '_item_view',
    'pager' => [
        'class' => \kop\y2sp\ScrollPager::className(),
        'negativeMargin' => '200',
        'triggerText' => 'Load More news',
        'triggerOffset' => 3,
        'noneLeftText' => '',
    ],
    'summary' => '',
])
?>