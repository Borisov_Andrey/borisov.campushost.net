<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use \app\models\Users;

AppAsset::register($this);

$user = \app\components\helpers\UserHelper::getAuthUser();
$guest_channel = (!$user) ? 'guest_channel_'.\Yii::$app->session->getId() : false;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

    <script src="https://js.pusher.com/3.2/pusher.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">



    <script>

        function modalShow(message){
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "50000",
                "extendedTimeOut": "50000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.info(message);
        }
        // Enable pusher logging - don't include this in production
        //Pusher.logToConsole = true;

        var pusher = new Pusher('5955a30fac303a448eff', {
            encrypted: true
        });


        <?php if (!$user) : ?>
        var common_guest_channel = pusher.subscribe('common_guest_channel');
        common_guest_channel.bind('my_event', function(data) {
            modalShow(data.message);
        });
        <?php endif?>


        <?php if ($guest_channel) : ?>
        var guest_channel = pusher.subscribe('<?= $guest_channel ?>');
        guest_channel.bind('my_event', function(data) {
            modalShow(data.message);
        });
        <?php endif?>

        <?php if ($user && $user -> profile -> push_notification) : ?>
            var private_channel = pusher.subscribe('private_channel_<?= $user -> id ?>');
            private_channel.bind('my_event', function(data) {
                modalShow(data.message);
            });
        <?php endif?>


        <?php if ($user &&  $user -> profile -> push_notification && ($user -> role_id == Users::ROLE_USER || $user -> role_id == Users::ROLE_EDITOR)) : ?>
        var group_channel = pusher.subscribe('group_channel');
        group_channel.bind('my_event', function(data) {
            modalShow(data.message);
        });
        <?php endif?>


        <?php if ($user && $user -> role_id == Users::ROLE_ADMIN) : ?>
        var admins_channel = pusher.subscribe('admins_channel');
        admins_channel.bind('my_event', function(data) {
            modalShow(data.message);
        });
        <?php endif?>

    </script>




</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('Cm', 'News'),'url' => ['/']],
            ['label' => Yii::t('Cm', 'Registration'), 'url' => ['/site/registration']],
            ['label' => Yii::t('Cm', 'Restore Password'), 'url' => ['/site/restore-password']],
        //    ['label' => 'About', 'url' => ['/site/about']],
        //    ['label' => 'Contact', 'url' => ['/site/contact']],
            (!empty(Yii::$app->user->identity->role_id) && Yii::$app->user->identity->role_id == 1) ? (
            ['label' => Yii::t('Cm', 'CP'), 'url' => ['/cp/news']]
            ) : (''),

            (!empty(Yii::$app->user->identity->role_id) && Yii::$app->user->identity->role_id == 3) ? (
            ['label' => Yii::t('Cm', 'Profile'), 'url' => ['/user/profile']]
            ) : (''),


            Yii::$app->user->isGuest ? (
                ['label' => Yii::t('Cm', 'Enter'),  'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    Yii::t('Cm', 'Logout').' (' . Yii::$app->user->identity->login . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            ),
        ],
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
