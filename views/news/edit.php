<?php
    use yii\helpers\Url;
?>

<?= \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/title.php'); ?>

<div class="row">
        <?=  \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/form_vertical_title.php', ['title' => $this->params['breadcrumbs'][0]['label']]);?>
        <?=  \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/form_vertical.php', ['model' => $model]);?>
</div>