<?php

use yii\widgets\LinkPager;
use \yii\helpers\Html;
use \yii\helpers\Url;
?>
<?= \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/title.php'); ?>
<div class="well row">
    <div><?= $news -> name?></div>
    <hr>
    <div><?= $news -> content ?></div>
</div>
