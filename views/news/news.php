<?php

use yii\widgets\LinkPager;
use \yii\helpers\Html;
use \yii\helpers\Url;
?>

<?= \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/views/common/title.php'); ?>
<?php if ($can_add) : ?>
  <div class="text-right">
        <?= Html::a(Yii::t('Cm', 'Add news'),  Url::to('/news/add', true), ['class' => 'btn btn-success']) ?>
  </div>
<?php endif ?>
<?php foreach ($models as $model) : ?>
    <div class="row well">
        <div>
            <?php if ($can_view) : ?>
                <?= Html::a($model ->  name , Url::to('/news/view/'.$model ->  url, true)) ?>
            <?php else : ?>
                <?= $model ->  name ?>
            <?php endif ?>
            <?php if ($can_edit) : ?>
                <div class="pull-right">
                    <?= Html::a( \Yii::t('Cm', 'Edit') , Url::to('/news/edit/'.$model ->  id, true), ['class' => 'btn btn-success']) ?>
                </div>
            <?php endif ?>
        </div>
        <hr>
        <div><?= $model ->  preview ?></div>
    </div>
<?php endforeach ?>


<?= LinkPager::widget([
    'pagination' => $pages,
]);
?>