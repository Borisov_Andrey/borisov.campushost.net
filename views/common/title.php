<?php
    $breadcrumbs = (!empty($this->params['breadcrumbs'])) ? $this->params['breadcrumbs'] : false;

    $title = false;
    if($breadcrumbs){
        $breadcrumbs = array_reverse($breadcrumbs);
        foreach ($breadcrumbs as $item){
            $title .= $item['label'].' :: ';
        }
    }
    $title .= ' Тестовая задача';
    $this -> title = $title;
?>