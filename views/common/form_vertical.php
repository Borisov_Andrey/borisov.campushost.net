<?php
    use kartik\widgets\ActiveForm;
    use kartik\form\ActiveField;
    use kartik\builder\Form;
    use yii\helpers\Html;
use \app\components\helpers\YiiFormHtmlHelper;
?>

<?php $form = ActiveForm::begin([
    'type'=>ActiveForm::TYPE_VERTICAL,
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
]); ?>

    <?php
        $class = !(empty($class)) ? $class : false;
        $columns = !(empty($columns)) ? $columns : 1;
    ?>



    <?php if (isset($model) && is_object($model)) : ?>
        <?= YiiFormHtmlHelper::buildForm($model, $form, $columns, $class) ?>
    <?php endif ?>

    <?php if (isset($models) && is_array($models)) : ?>
        <?php foreach ($models as $model) : ?>
            <?php if(is_object($model)) : ?>
                <?= YiiFormHtmlHelper::buildForm($model, $form, $columns, $class) ?>
            <?php endif?>
        <?php endforeach ?>
    <?php endif ?>

<?php ActiveForm::end();?>