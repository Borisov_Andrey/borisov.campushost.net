<?php
return [
    'Save' => 'Зберегти',
    'Enter' => 'Увійти',
    'Remember Me' => 'Запам\'ятати мене',
    'Password' => 'Пароль',
    'Note' => 'Довідка',
    'Enter email' => 'Вкажіть email',
    'Enter password' => 'Вкажіть пароль',
    'Example' => 'Наприклад',
    'Do not share your password' => 'Нікому не повідомляйте Ваш пароль',

    'Register' => 'Зареєструватися',
    'remember Me' => 'Запам\'ятати мене',
    'Invalid login or(and) password' => 'Невірний логін або(і) пароль',

    'Registration' => 'Реєстрація',
    'Restore password' => 'Відновлення паролю',
    'Restore registration' => 'Відновлення реєстрації',
];