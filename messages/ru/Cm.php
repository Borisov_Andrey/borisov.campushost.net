<?php
return [
    'Login' => 'Email',

    'Hello for user' => 'Здравствуйте, {name}',

    'Save' => 'Сохранить',
    'Enter' => 'Вход',
    'Remember Me' => 'Запомнить меня',
    'Password' => 'Пароль',
    'Note' => 'Справка',
    'Enter email' => 'Укажите email',
    'Enter password' => 'Укажите пароль',
    'Repeat password' => 'Повторите пароль',
    'Restore Password' => 'Восстановить пароль',
    'Example' => 'Например',
    'Do not share your password' => 'Никому не сообщайте Ваш пароль',

    'Hash' => 'Код активации',
    'Enter hash' => 'Укажите код активации',
    'Activation' => 'Активация аккаунта',
    'Hash not find' => 'Код активации не найден',
    'Email not find' => 'Email не найден',

    "Passwords don\'t match" => 'Пароли не совпадают',
    'New password' => 'Новый пароль',

    'Register' => 'Зарегистрироваться',
    'Remember Me' => 'Запомнить меня',
    'Invalid login or(and) password' => 'Неверный логин или(и) пароль',

    'Registration' => 'Регистрация',
    'Restore password' => 'Восстановление пароля',
    'Restore registration' => 'Восстановление регистрации',

    'Submit' => 'Отослать',
    'News' => 'Новости',
    'Add news' => 'Добавить новость',
    'Edit' => 'Редактировать',

    'Name' => 'Название',
    'Preview' => 'Превью',
    'Content' => 'Контент',

    'Control panel' => 'Панель управления',
    'News access' => 'Права доступа новостной ленты',
    'List' => 'Список',
    'Add' => 'Создать',
    'View' => 'Просмотр',
    'Save' => 'Сохранить',

    'Home' => 'Главная',
    'CP' => 'ПУ',
    'Logout' => 'Выход',
    'Enter' => 'Вход',
    'Profile' => 'Профайл',

    'First Name' => 'Имя',
    'Last Name' => 'Фамилия',
    'Email Notification' => 'Email уведомления',
    'Push Notification' => 'Push уведомления',


    'Role' => 'Роль',
    'Select a role' => 'Укажите роль',
    'Account status' => 'Статус аккаунта',
    'Select a status' => 'Укажите статутс',

    'Created At' => 'Создан',
    'Updated At' => 'Обновлен',
    'Last Active At' => 'Последняя авторизация',
    'User' => 'Пользователь',
    'Update' => 'Редактировать',
    'Delete' => 'Удалить',
    'Create Users' => 'Создать пользователя',
    'Users' => 'Пользователи',
    'Update user #' => 'Редактировать пользователя #',
    'Update News: ' => 'Редактировать новость: ',
    'Accesses' => 'Права доступа',
    'Settings' => 'Настройки',

    'Email templates' => 'Шаблоны писем',

    'Enter first name' => 'Укажите имя',
    'Enter last name' => 'Укажите фамилию',
    'Example hit' => 'Пример подсказки',

    'Enter name' => 'Укажите название',
    'Enter preview' => 'Укажите превью',
    'Enter content' => 'Укажите контент',

    'Activate account' => 'Активация аккаунта',
    'New password' => 'Новый пароль',
    'To continue registration, please, check you email and activate account' => 'Вам неоходимо активировать аккаунт, пожалуйста, проверьте вашу почту, и следуйте иструкциям в письме.',
    'We have news for you' => 'У нас есть новости для вас',

    'Templates' => 'Шаблон',
    'Event' => 'Событие',
    'Email Templates' => 'Шаблоны писем',
    'Create Email Templates' => 'Создать шаблон письма',
    'Create' => 'Создать',

    'New user has been activated account' => 'Новый пользователь активировал аккаунт',
    'Congratulation, you account has been activated' => 'Поздравляем, ваш аккаунт был активирован',

    'Set new password' => 'Укажите новый пароль',
    'Create News' => 'Создать новость',
    'Update Email Templates: ' => 'Редактировать шаблон письма: ',
    'Value' => 'Значение',
    'News Configs' => 'Настройки новостей',
    'Create News Config' => 'Создать настройку новостей',
    'Update News Config: ' => 'Редактировать насройку новостей: ',

    'administrators' => 'Администраторы',
    'editors' => 'Редакторы',
    'users' => 'Авторизированные пользователи',
    'guests' => 'Гости',
    'active' => 'Активный',
    'block' => 'Заблокированный'

];
