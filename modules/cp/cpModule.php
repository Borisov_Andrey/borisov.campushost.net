<?php

namespace app\modules\cp;
use app\models\Users;
use Yii;
use yii\web\ConflictHttpException;

/**
 * cpModule module definition class
 */
class cpModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\cp\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {

        if(Yii::$app->user->isGuest || empty(Yii::$app->user->identity->role_id) || (Yii::$app->user->identity->role_id != Users::ROLE_ADMIN)){
            throw new ConflictHttpException('Access denided');
        }
        parent::init();
    }
}
