<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('Cm', 'News access');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div>
    <?php if ($settings) : ?>
        <?php $form = ActiveForm::begin(); ?>
        <div class="well">
            <?php foreach ($settings as $item) : ?>
                <div class="row">
                    <div class="col-md-4"><?= \Yii::t('Cm', $item->role->name) ?></div>
                    <div class="col-md-2"><?= $form->field($item, 'list')->checkbox(['name' => 'NewsSettings['.$item->role_id.'][list]']) ?></div>
                    <div class="col-md-2"><?= $form->field($item, 'view')->checkbox(['name' => 'NewsSettings['.$item->role_id.'][view]']) ?></div>
                    <div class="col-md-2"><?= $form->field($item, 'edit')->checkbox(['name' => 'NewsSettings['.$item->role_id.'][edit]']) ?></div>
                    <div class="col-md-2"><?= $form->field($item, 'add')->checkbox(['name' => 'NewsSettings['.$item->role_id.'][add]']) ?></div>
                </div>
            <?php endforeach ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('Cm', 'Save'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    <?php endif?>
</div>
