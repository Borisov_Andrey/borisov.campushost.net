<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = Yii::t('Cm', 'Рассылка уведомлений');
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
?>
<h2><?= $this->title ?></h2>

<div>
    <?php $form = ActiveForm::begin(); ?>
    <div class="well">
        <div class="well">
            <div class="radio">
                <label><input  type="radio" name="to_type" value="to_groups" checked>Группам </label>
            </div>
            <?php foreach ($roles as $role) : ?>
                <div class="checkbox">
                    <label><input  type="checkbox" name="to_role[<?= $role -> id ?>]" value="<?= $role -> id ?>"><?= \Yii::t('Cm', $role -> name );?> </label>
                </div>
            <?php endforeach ?>
        </div>

    <div class="well">
        <div class="radio">
            <label><input  type="radio" name="to_type" value="to_users">Персонально</label>
        </div>

            <?=  $form->field($model, 'users')->widget(Select2::classname(), [
                'data' =>  \app\components\helpers\RangeHelper::getSelectFromModel(new \app\models\Users(), 'id', 'login'),
                'options' => ['placeholder' => 'Укажите email(s) ...', 'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true,
                    'tags' => false,
                    'maximumInputLength' => 100
                ]
            ])->label('');    ?>
    </div>

    <div class="form-group well">
        <label for="usr">Сообщение:</label>
        <textarea name="msg" class="form-control"></textarea>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('Cm', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php if ($sended):  ?>
        <div class="alert alert-success">
            <strong>Успех!</strong> Сообщения были разосланны.
        </div>
    <?php endif ?>
    <?php ActiveForm::end(); ?>
</div>
