<footer class="footer">
    <div class="container text-center">
        <div class="col-md-6">
            &copy; <?= Yii::t('app','Company name') ?> <?= date('Y') ?>
        </div>
        <div class="col-md-6">
            <?= Yii::t('app','Company motto') ?>
        </div>
    </div>
</footer>