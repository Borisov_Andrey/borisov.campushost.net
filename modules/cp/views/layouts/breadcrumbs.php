<?= \Yii::$app->controller->renderFile(\Yii::$app->basePath . '/modules/cp/views/layouts/title.php'); ?>

<h1>

    <?php if(!empty($this->params['breadcrumbs'][0]['label'])) : ?>
        <?= $this->params['breadcrumbs'][0]['label'] ?>
    <?php endif ?>
    <!--<small>Optional description</small>-->
</h1>

<?= yii\widgets\Breadcrumbs::widget([
    'homeLink' => [
        'label' => $this->title = Yii::t('Cm', 'Control panel'),
        'url' => '/cp/index',
    ],
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>

