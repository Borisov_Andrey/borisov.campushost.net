<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('Cm', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('Cm', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('Cm', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'login',
           // 'password',
            [
                'label' => \Yii::t('Cm', 'Role'),
                'format' => 'html',
                'value' => \Yii::t('Cm', $model -> role -> name )
            ],
            [
                'label' => \Yii::t('Cm', 'Account status'),
                'format' => 'html',
                'value' => \Yii::t('Cm', $model -> accountStatus -> name )
            ],
            'hash',
            'last_active_at',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
