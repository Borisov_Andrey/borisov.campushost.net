<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \app\components\helpers\RangeHelper;
use kartik\select2\Select2;
use \app\models\UsersRoles;
use \app\models\AccountStatuses;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('Cm', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('Cm', 'Create Users'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'login',
         //   'password',
            [
                'attribute'=>'role_id',
                'label' => \Yii::t('Cm', 'Role'),
                'contentOptions'=>['style'=>'max-width: 150px; text-align:center'],
                'filterOptions' => ['style' => 'max-width: 150px;'],
                'content'=>function($model){
                    return \Yii::t('Cm', $model -> role -> name );
                },
                'filter' => Select2::widget([
                    'name' => 'UsersSearch[role_id]',
                    'value' => !(empty($searchModel-> role_id)) ? $searchModel-> role_id : '',
                    'data' =>  RangeHelper::getSelectFromModel( new UsersRoles(), 'id', 'name', false, 'id'),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => \Yii::t('Cm', 'Select a role'),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],

                ]),
            ],
            [
                'attribute'=>'account_status_id',
                'label' => \Yii::t('Cm', 'Account status'),
                'contentOptions'=>['style'=>'max-width: 150px; text-align:center'],
                'filterOptions' => ['style' => 'max-width: 150px;'],
                'content'=>function($model){
                    return \Yii::t('Cm', $model -> accountStatus -> name );
                },
                'filter' => Select2::widget([
                    'name' => 'UsersSearch[account_status_id]',
                    'value' => !(empty($searchModel-> account_status_id)) ? $searchModel-> account_status_id : '',
                    'data' =>  RangeHelper::getSelectFromModel( new AccountStatuses(), 'id', 'name'),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => \Yii::t('Cm', 'Select a status'),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],

                ]),
            ],
            // 'hash',
            // 'last_active_at',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
