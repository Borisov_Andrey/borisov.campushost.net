<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\components\helpers\RangeHelper;
use kartik\select2\Select2;
use \app\models\UsersRoles;
use \app\models\AccountStatuses;
/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin([
      //  'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'validateOnSubmit' => true
    ]); ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'autocomplete' => 'off']) ?>

    <?=  $form->field($model, 'role_id')->widget(Select2::classname(), [
        'data' =>  RangeHelper::getSelectFromModel(new UsersRoles(), 'id', 'name', false, 'id'),
        'options' => ['placeholder' => \Yii::t('Cm', 'Select a role'), 'multiple' => false],
        'pluginOptions' => [
            'allowClear' => true,
            'tags' => false,
            'maximumInputLength' => 10
        ]
    ])->label(\Yii::t('Cm', 'Role'));
    ?>


    <?=  $form->field($model, 'account_status_id')->widget(Select2::classname(), [
        'data' =>  RangeHelper::getSelectFromModel(new AccountStatuses(), 'id', 'name'),
        'options' => ['placeholder' => \Yii::t('Cm', 'Select a status'), 'multiple' => false],
        'pluginOptions' => [
            'allowClear' => true,
            'tags' => false,
            'maximumInputLength' => 10
        ]
    ])->label(\Yii::t('Cm', 'Account status'));
    ?>

    <?= $form->field($model, 'hash')->textInput(['maxlength' => true, 'readonly' => true]) ?>

    <?= $form->field($model, 'last_active_at')->textInput(['readonly' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput(['readonly' => true]) ?>

    <?= $form->field($model, 'updated_at')->textInput(['readonly' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('Cm', 'Create') : Yii::t('Cm', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
