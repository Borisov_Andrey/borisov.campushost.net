<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NewsConfig */

$this->title = Yii::t('Cm', 'Update News Config: ', [
    'modelClass' => 'News Config',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('Cm', 'News Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('Cm', 'Update');
?>
<div class="news-config-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
