<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\NewsConfig */

$this->title = Yii::t('app', 'Create News Config');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-config-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
