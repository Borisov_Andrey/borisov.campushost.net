<?php

namespace app\modules\cp\controllers;

use app\models\NewsSettings;

class NewsAccessController extends CpController
{
    public function actionIndex()
    {
        if (\Yii::$app->request->post()){
            $post = \Yii::$app->request->post("NewsSettings");

            foreach ($post as $key => $item){
                $settings = NewsSettings::findOne(['role_id' => $key]);
                $settings -> setAttributes($item);
                $settings -> save();
            }
            return $this->redirect('/cp/news-access');
        }

        $settings = NewsSettings::find()->all();
        return $this->render('index', ['settings' => $settings]);
    }
}
