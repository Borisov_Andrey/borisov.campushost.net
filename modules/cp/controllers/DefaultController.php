<?php

namespace app\modules\cp\controllers;

use yii\web\Controller;

/**
 * Default controller for the `cpModule` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTest()
    {
        die('test');
        return $this->render('index');
    }

}
