<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 04.01.2016
 * Time: 12:28
 */

namespace app\modules\cp\controllers;


use yii\web\Controller;
use \Yii;


class CpController  extends Controller {
    public $layout='index';

       public function actions()
    {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


}