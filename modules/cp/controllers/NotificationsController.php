<?php

namespace app\modules\cp\controllers;

use app\components\helpers\NotificationHelper;
use app\models\CpNotificationForm;
use app\models\LoginForm;
use app\models\UsersRoles;

class NotificationsController extends CpController
{
    public function actionIndex()
    {

        $sended = false;

        if (\Yii::$app->request->post()){
            $post = \Yii::$app->request->post();
            $to_type = \Yii::$app->request->post('to_type');
            $msg = $post["msg"];
            if ($to_type == 'to_users'){
                $users = $post['CpNotificationForm']["users"];
                if ($users){
                    foreach ($users as $user_id){
                        NotificationHelper::pusherSendMsg('private_channel_'.$user_id, $msg);
                    }
                }

                $sended = true;

            }else if($to_type == 'to_groups'){
                $to_role = $post["to_role"];
                if ($to_role)
                    foreach ($to_role as $role_id){
                        if($role_id == 4){
                            NotificationHelper::pusherSendMsg('common_guest_channel', $msg);
                        }
                        if($role_id == 1){
                            NotificationHelper::pusherSendMsg('admins_channel', $msg);
                        }
                        if($role_id == 2 || $role_id == 3){
                            NotificationHelper::pusherSendMsg('group_channel', $msg);
                        }
                    }
                $sended = true;
            }

        }

        $model = new CpNotificationForm();

        $roles = UsersRoles::find()->all();
        return $this->render('index', ['roles' => $roles, 'model' => $model, 'sended' =>$sended]);
    }



}
