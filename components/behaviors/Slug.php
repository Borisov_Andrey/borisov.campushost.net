<?php

namespace app\components\behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;
use dosamigos\transliterator\TransliteratorHelper;

class Slug  extends Behavior
{
    public $in_attribute = 'name';
    public $out_attribute = 'slug';

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
            ActiveRecord::EVENT_AFTER_INSERT =>  'afterInsert',
        ];
    }

    public function beforeUpdate($event)
    {
        $this->owner->{$this->out_attribute} = $this -> toSlug($this->owner->{$this->in_attribute}, $this->owner->id);
    }

    public function afterInsert($event)
    {
        $this->owner->{$this->out_attribute} = $this -> toSlug($this->owner->{$this->in_attribute}, $this->owner->id);
        $this->owner->save();
    }

    private function toSlug($str,$id){
        $slug = TransliteratorHelper::process($str, '-', 'en');
        $slug = preg_replace('|\s+|', ' ', trim($slug));
        $slug = strtolower(str_replace(' ', '-',$slug));
        $slug .= '-'.$id;
        return $slug;
    }

}