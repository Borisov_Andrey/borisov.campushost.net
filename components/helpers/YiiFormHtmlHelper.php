<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 30.10.2015
 * Time: 10:54
 */

namespace app\components\helpers;

use yii\web\ForbiddenHttpException;
use kartik\form\ActiveField;

class YiiFormHtmlHelper
{
    public static function getInput($params = false){

        return $description = \Yii::$app->controller->renderFile(__DIR__ . '/views/YiiFormHtmlHelper/input.php', ['params' => $params]);
    }

    public static function getRadio($params = false){

        if (empty($params['section']['class'])){
            $params['section']['class'] = 'col-4';
        }

        return $description = \Yii::$app->controller->renderFile(__DIR__ . '/views/YiiFormHtmlHelper/radio.php', ['params' => $params]);

    }

    public static function getRadioItem($index, $label, $name, $checked, $value){
        return $description = \Yii::$app->controller->renderFile(__DIR__ . '/views/YiiFormHtmlHelper/radio_item.php', ['index' => $index, 'label' => $label, 'name' => $name, 'checked' => $checked, 'value' => $value]);
    }


    public static function getSelect($params = false){

        if (empty($params['section']['class'])){
            $params['section']['class'] = 'col-4';
        }

        return $description = \Yii::$app->controller->renderFile(__DIR__ . '/views/YiiFormHtmlHelper/select.php', ['params' => $params]);

    }

    public static function getTextArea($params = false){

        return $description = \Yii::$app->controller->renderFile(__DIR__ . '/views/YiiFormHtmlHelper/textarea.php', ['params' => $params]);
    }


    public static function buildForm($model, \kartik\widgets\ActiveForm $form, $columns = 1, $class = false, $id = false ){

        if (!is_object($model)){
            throw new InvalidArgumentException('Model is not objest');
        }

        $columns = ((int)$columns >= 1)  ? $columns : 1;

        return
            \kartik\builder\Form::widget([
                'model'=>$model,
                'id' => $id,
                'form'=>$form,
                'columns'=>$columns,
                'attributes'=>$model->formAttribs,
                'options' => [
                    'class' => $class
                ]
            ]);
    }

    public static function buildField($type, $items = false, $placeholder = false, $hint = false, $icon = false){

        $res['type'] = $type;

        if ($items) {
            $res['items'] = $items;
        }

        if ($hint) {
            $res['hint'] = $hint;
            $res['fieldConfig'] =
                [
                    'hintType' => ActiveField::HINT_SPECIAL,
                    'hintSettings' => [
                        'showIcon' => true,
                        'onLabelClick' => true,
                        'onLabelHover' => false,
                        'title' => '<i class="glyphicon glyphicon-info-sign"></i> ' . \Yii::t('Cm', 'Note'),
                    ],
                ];
        }

        if ($placeholder)
            $res['options'] =[
                'placeholder'=>$placeholder,
            ];

        if ($icon)
            $res['fieldConfig']['feedbackIcon'] = [
                'default' => $icon,
                'success' => 'ok',
                'error' => 'exclamation-sign',
                'defaultOptions' => ['class'=>'']
            ];

        return $res;
    }

    public static function oneBuildField($hint = false, $icon = false)
    {
        $res = [];

        if ($hint) {
            $res['fieldConfig'] =
                [
                    'hintType' => ActiveField::HINT_SPECIAL,
                    'hintSettings' => [
                        'placement' => 'top',
                        'showIcon' => true,
                        'onLabelClick' => true,
                        'onLabelHover' => false,
                        'title' => '<i class="glyphicon glyphicon-info-sign"></i> ' . \Yii::t('Cm', 'Note'),
                    ],
                ];
        }

        if ($icon)
            $res['fieldConfig']['feedbackIcon'] = [
                'default' => $icon,
                'success' => 'ok',
                'error' => 'exclamation-sign',
                'defaultOptions' => ['class'=>'']
            ];

        return $res["fieldConfig"];
    }
}