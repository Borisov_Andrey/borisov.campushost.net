<?php
namespace app\components\helpers;

use app\models\News;
use app\models\Users;

class NotificationHelper{

    public static function pusherSendTestMsg($channel, $message){
        \Yii::$app->session->open();
        $channel = ($channel == 'private_channel') ? $channel.'_'.\Yii::$app->user->id : $channel;
        $channel = ($channel == 'guest_channel') ? $channel.'_'.\Yii::$app->session->getId() : $channel;

        $data['message'] = $message;
        return \Yii::$app->pusher->trigger($channel, 'my_event', $data );
    }

    public static function pusherSendMsg($channel, $message){
        require_once($_SERVER['DOCUMENT_ROOT'].'/../vendor/pusher/pusher-php-server/lib/Pusher.php');

        $options = array(
            'encrypted' => true
        );
        $pusher = new \Pusher(
            '5955a30fac303a448eff',
            '474f8d07005e4bccc5b2',
            '265848',
            $options
        );

        \Yii::$app->session->open();
        $channel = ($channel == 'private_channel') ? $channel.'_'.\Yii::$app->user->id : $channel;
        $channel = ($channel == 'guest_channel') ? $channel.'_'.\Yii::$app->session->getId() : $channel;
        $data['message'] = $message;
        return  $pusher->trigger($channel, 'my_event', $data);
    }

    public static function sendAfterRegistration($user_id, $password){
        self::pusherSendMsg('guest_channel',  \Yii::t('Cm', 'To continue registration, please, check you email and activate account'));
        EmailHelper::sendAfterRegistration($user_id, $password);
    }

    public static function sendAfterActivationAccount(){
        self::pusherSendMsg('guest_channel',  \Yii::t('Cm', 'Congratulation, you account has been activated'));
        self::pusherSendMsg('admins_channel',  \Yii::t('Cm', 'New user has been activated account'));
    }

    public static function sendAfterAddNews(News $news){
        $msg = "Новость <a href='/news/view/".$news -> url."'>".$news -> name."</a> была создана";
        self::pusherSendMsg('group_channel',  $msg);
        self::pusherSendMsg('admins_channel',  $msg);
        EmailHelper::sendAfterAddNews($news);
    }

    public static function sendAfterRestorePassword(Users $user){
        self::pusherSendMsg('admins_channel',  \Yii::t('Cm', 'Пользователь '.$user -> login.' восстанавливает пароль'));
        EmailHelper::sendAfterRestorePassword($user);
    }

    public static function sendAfterNewPassword(Users $user, $password){
        self::pusherSendMsg('admins_channel',  \Yii::t('Cm', 'Пользователь '.$user -> login.' задал новый пароль'));
        EmailHelper::sendAfterNewPassword($user, $password);
    }
}