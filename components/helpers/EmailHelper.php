<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 19.11.2015
 * Time: 12:03
 */

namespace app\components\helpers;

use app\models\EmailTemplates;
use app\models\News;
use app\models\Users;
use Mailgun\Mailgun;
use \yii\web\ConflictHttpException;


class EmailHelper
{
    private $emailObject = null;

    public static function init()
    {
        $object = new EmailHelper();
        $object->emailObject = new Mailgun(\Yii::$app->params['mg-api-key']);

        return $object;
    }

    public function send($emailTo, $subject, $html)
    {

        try {

            return \Yii::$app->mailer->compose()
            ->setFrom(\Yii::$app->params['support_email'])
            ->setTo($emailTo)
            ->setSubject($subject)
            ->setHtmlBody($html)
            ->send();
        } catch (Exception $e) {

        }
    }

    public function sendViewMail($emailTo, $subject, $viewFile, array $params)
    {
        \Yii::$app->controller->layout = 'mail';
        
        $params['subject'] = $subject;
        $rendered_content = \Yii::$app->controller->render('/emails/'.$viewFile .'.php', $params);
        
        \Yii::$app->mailer->compose()
            ->setFrom(\Yii::$app->params['support_email'])
            ->setTo($emailTo)
            ->setSubject($subject)
            ->setHtmlBody($rendered_content)
            ->send();

        return true;
    }

    public static function sendAfterRegistration($user_id, $password){
        $user = Users::findOne($user_id);
        $template = EmailTemplates::findOne(['event' => 'after_user_registration'])-> templates;
        if (!$template){
            return false;
        }

        $template = str_replace("%%login%%", $user -> login, $template);
        $template = str_replace("%%hash%%", $user -> hash, $template);
        $template = str_replace("\n","<br>", $template);

        return self::init()->init()->send($user -> login,  \Yii::t('Cm', 'Activate account'), $template);
    }

    public static function sendAfterRestorePassword(Users $user){
        $template = EmailTemplates::findOne(['event' => 'after_user_restore_password'])-> templates;
        if (!$template){
            return false;
        }

        $template = str_replace("%%login%%", $user -> login, $template);
        $template = str_replace("%%hash%%", $user -> hash, $template);
        $template = str_replace("\n","<br>", $template);

        return self::init()->init()->send($user -> login,  \Yii::t('Cm', 'Set new password'), $template);
    }

    public static function sendAfterNewPassword(Users $user, $password){
        $template = EmailTemplates::findOne(['event' => 'after_user_set_new_password'])-> templates;
        if (!$template){
            return false;
        }

        $template = str_replace("%%login%%", $user -> login, $template);
        $template = str_replace("\n","<br>", $template);

        return self::init()->init()->send($user -> login,  \Yii::t('Cm', 'New password'), $template);
    }

    public static function sendAfterAddNews(News $news){

        $template = EmailTemplates::findOne(['event' => 'after_add_news'])-> templates;
        if (!$template){
            return false;
        }

        $template = str_replace("%%url%%", $news -> url, $template);
        $template = str_replace("%%name%%", $news -> name, $template);
        $template = str_replace("\n","<br>", $template);

        $users = Users::find()->select('users.*, users_profiles.*')->joinWith('profile')
            ->where(['users_profiles.email_notification' => 1])
            ->all();

        foreach ($users as $user) {
            self::init()->init()->send($user->login, \Yii::t('Cm', 'We have news for you'), $template);
        }

        return true;
    }
}