<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25.12.14
 * Time: 19:07
 */

namespace app\components\helpers;

use app\models\DisplayStatuses;
use app\models\ExecutorAgreementsTypes;
use app\models\ExecutorLocations;
use app\models\ExecutorPaymentsTypes;
use app\models\GeoLocation;
use app\models\User;
use app\models\UserAccountInfo;
use app\models\UserCampaignInfo;
use app\models\UserList;
use app\models\UserPaymentsInfo;
use app\models\Users;
use app\models\UsersProfiles;
use app\models\UserTypes;
use \yii\web\ConflictHttpException;
use Yii;


class UserHelper
{


    public static function restorePasswordGenerateHash($email){
        if (!$email){
            throw new ConflictHttpException('Invalid email');
        }

        $user = Users::findByLogin($email);

        if (!$user){
            throw new ConflictHttpException('User not find');
        }

        $user -> hash = self::generateHash($user -> id);

        if (!$user -> validate()){
            throw new ConflictHttpException('Invalid model Users');
        }
        $user -> save();

        return $user;
    }


    private static function getSecretKey(){
        $secret_key = \Yii::$app->params['secret_key'];
        if (empty($secret_key)){
            throw new ConflictHttpException('Secret key not find');
        }
        return $secret_key;
    }


    public static function generatePassword($password){
        if (empty($password)){
            throw new ConflictHttpException('invalid password');
        }

        return Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public static function generateHash($user_id){
        if (!(int)$user_id){
            throw new ConflictHttpException('invalid user_id');
        }

        return md5(uniqid(self::getSecretKey()));
    }

    private static function validatePassword($password, $hash){

        if (empty($password)){
            throw new ConflictHttpException('invalid password');
        }

        if (empty($hash)){
            throw new ConflictHttpException('invalid hash');
        }


        return (Yii::$app->getSecurity()->validatePassword($password, $hash)) ? true : false;
    }


    public static function getByEmailAndPassword($email, $password){
        $user = Users::findByLogin($email);

        if (!$user){
            return false;
        }

        if (!self::validatePassword($password, $user -> password)){
            return false;
        }

        return $user;
    }


    public static function getByEmail($email){
        return Users::findByLogin($email);
    }


    public static function getByHash($hash){
        return Users::findOne(['hash' => $hash]);
    }


    public static function login($email, $password = false){

        $user = ($password) ? self::getByEmailAndPassword($email, $password) : self::getByEmail($email);

        if (!$user){
            return false;
        }

        Yii::$app->user->login($user, 1 ? 3600*24*30 : 0);
        return true;
    }


    public static function getUserById($user_id){
        return Users::findOne(['id' => $user_id]);
    }


    public static function getAuthUser(){
        $user_id = Yii::$app->user->id;
        if (!$user_id){
            return false;
        }

        $user = self::getUserById($user_id);

        return $user;
    }


    public static function LogOut(){
        return Yii::$app->user->logout();
    }

    public static function createUser($email, $password){

        $transaction = \Yii::$app->db->beginTransaction();
            try {

            $user = Users::findByLogin($email);
            if ($user){
                throw new ConflictHttpException('Email already exist');
            }

            $user = new Users();

            $user -> login =  $email;
            $user -> password = $password;
            $user -> account_status_id = 2;
            $user -> role_id = 3;

            if (!$user -> validate()){
                throw new ConflictHttpException('invalid model Users');
            }

            $user -> save();
            $user -> hash = self::generateHash($user -> id);
            $user -> save();

            $profile = new UsersProfiles();
            $profile -> user_id =  $user -> id;
            $profile -> email_notification = 1;
            $profile -> push_notification = 1;
            $profile -> save();

            $transaction->commit();

            return $user -> id;

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return true;
    }


    public static function activateAccount(Users $user){
        $user -> hash = '';
        $user -> account_status_id = 1;

        if (!$user -> validate()){
            throw new ConflictHttpException('invalid model Users');
        }

        $user -> save();

        return $user -> id;

    }


    public static function setNewPassword(Users $user, $new_password){

        $user -> hash = '';
        $user -> password = $new_password;

        if (!$user -> validate()){
            throw new ConflictHttpException('invalid model Users');
        }

        $user -> save();

        return $user -> id;
    }

    public static function getRole(){
        if (Yii::$app->user->isGuest)
            return Users::ROLE_GUEST;

        switch (Yii::$app->user->identity->role_id){
            case 1:
                return Users::ROLE_ADMIN;
                break;
            case 2:
                return Users::ROLE_EDITOR;
                break;
            case 3:
                return Users::ROLE_USER;
                break;
            default:
                return Users::ROLE_GUEST;
                break;
        }
    }
}