<?php

namespace app\components\helpers;

use Yii;
use yii\helpers\ArrayHelper;

class RangeHelper
{
    public static function getYesNoSelect(){
        return ['1' => Yii::t('app','No'), '2' => Yii::t('app','Yes')];
    }

    public static function getYesNoView($value){
        return ($value) ?  Yii::t('app','Yes') :  Yii::t('app','No');
    }

    public static function getSelectFromModel($model, $from = 'id', $to = 'id', $prepare_not_select = false, $order_by = false){
        $order_by = ($order_by) ? $order_by : $to;
        $prepare = [NULL => 'Not select'];
        $result = ArrayHelper::map($model::find()->orderBy($order_by)->all(), $from, $to);

        if ($result){
            foreach ($result as &$item){
                $item = \Yii::t('Cm', $item );
            }
        }

        return ($prepare_not_select) ? ArrayHelper::merge($prepare, $result) : $result;
    }

    public static function range($start, $end){
        $range = array();
        foreach(range($start, $end) as $value){
            $range[$value] = $value;
        }
        return $range;
    }
}