<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 30.10.2015
 * Time: 10:57
 */

?>
<div class="row">
    <div class="col-xs-12">
        {label}
        <?php if (!empty($params['hint'])) : ?>
            <span class="hint glyphicon glyphicon-info-sign"></span>
            <div class="hint-block"><?= $params['hint'] ?></div>
        <?php endif?>
    </div>

    <div class="col-xs-12 text-right"></i>{error}</div>
    <div class="col-xs-12">{input}</div>
</div>
