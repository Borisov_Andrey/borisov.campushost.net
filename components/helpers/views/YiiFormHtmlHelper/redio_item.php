<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 30.10.2015
 * Time: 11:50
 */
use yii\helpers\Html;

?>
<label class="radio"><?= Htm::radio($name, $checked, ['value' => $value]) ?> <i></i><span> <?= $label ?> </span></label>