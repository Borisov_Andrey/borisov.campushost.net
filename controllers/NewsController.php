<?php

namespace app\controllers;

use app\components\events\EventUser;
use app\components\helpers\EmailHelper;
use app\components\helpers\UserHelper;
use app\models\News;
use app\models\NewsConfig;
use app\models\NewsSettings;
use app\models\Users;
use kartik\form\ActiveForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\ConflictHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\Pagination;

class NewsController extends Controller
{


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }



    public function actionList(){

/*
        for ($i = 1; $i <= 100; $i++ ){
            $news = new News();

            $news -> name = 'Название новости '.$i;
            $news -> preview = 'Превью новости '.$i. ' превью новости '.$i. 'превью новости '.$i. 'превью новости '.$i;
            $news -> content = 'Основной контент новости '.$i. ' основной контент новости'.$i. ' основной контент новости'.$i. ' основной контент новости'.$i. ' основной контент новости'.$i. ' основной контент новости'.$i. ' основной контент новости'.$i. ' основной контент новости'.$i. ' основной контент новости'.$i. ' основной контент новости'.$i. ' основной контент новости'.$i. ' основной контент новости'.$i. ' основной контент новости'.$i. ' основной контент новости';

            $news -> save();
        }

        die();
*/

        $can_list = NewsSettings::findOne(['role_id' =>  UserHelper::getRole(), 'list' => 1]);


        if(!$can_list){
            throw new ConflictHttpException('ERROR: access denied');
        }


        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => \Yii::t('Cm', 'News')
        ];



        $query = News::find();
        $countQuery = clone $query;
        $count_record_on_page = NewsConfig::findOne(['name' => 'count_record_on_page']);
        $page_size = !empty($count_record_on_page['value']) ? $count_record_on_page['value'] : NewsConfig::DEFAULT_COUNT_RECORDS_ON_PAGE;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => $page_size]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('news', [
            'models' => $models,
            'pages' => $pages,
            'can_add' => NewsSettings::findOne(['role_id' =>  UserHelper::getRole(), 'add' => 1]),
            'can_edit' => NewsSettings::findOne(['role_id' => UserHelper::getRole(), 'edit' => 1]),
            'can_view' => NewsSettings::findOne(['role_id' => UserHelper::getRole(), 'view' => 1]),
        ]);

    }

    public function actionView($slug){

        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => \Yii::t('Cm', 'News'),
             'url' => ['/news/list']
        ];

        $can_view = NewsSettings::findOne(['role_id' =>  UserHelper::getRole(), 'view' => 1]);

        if(!$can_view){
            throw new ConflictHttpException('ERROR: access denied');
        }


        $news = News::findOne(['url' => $slug]);
        if (!$news){
            throw new ConflictHttpException('News not find');
        }

        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => $news -> name
        ];

        return $this->render('view', [
            'news' => $news,
        ]);
    }

    public function actionEdit($id){

        $can_edit = NewsSettings::findOne(['role_id' =>  UserHelper::getRole(), 'edit' => 1]);

        if(!$can_edit){
            throw new ConflictHttpException('ERROR: access denied');
        }

        $model = News::findOne(['id' => $id]);
        $model -> scenario = News::SCENARIO_EDIT;

        if (!$model){
            throw new ConflictHttpException('News not find');
        }

        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => \Yii::t('Cm', 'News'),
            'url' => ['/news/list']
        ];

        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => 'Редактировать новость: '.$model -> name
        ];

        if (\Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if(\Yii::$app->request->isPost  && $model->load(Yii::$app->request->post())) {

            if (!$model->validate()){
                throw new ConflictHttpException('ERROR: Invalid model');
            }
            $model->save();
            return $this->redirect(Url::toRoute('/news/view/'.$model->url));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    public function actionAdd(){
        $can_add = NewsSettings::findOne(['role_id' =>  UserHelper::getRole(), 'add' => 1]);

        if(!$can_add){
            throw new ConflictHttpException('ERROR: access denied');
        }

        $model = new News();
        $model -> scenario = News::SCENARIO_ADD;

        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => \Yii::t('Cm', 'News'),
            'url' => ['/news/list']
        ];

        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => 'Создать новость'
        ];

        if (\Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if(\Yii::$app->request->isPost  && $model->load(Yii::$app->request->post())) {

            if (!$model->validate()){
                throw new ConflictHttpException('ERROR: Invalid model');
            }
            $model->save();
            return $this->redirect(Url::toRoute('/news/view/'.$model->url));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    public function actionRemove(){

    }
}
