<?php

namespace app\controllers;

use \Yii;
use app\components\helpers\UserHelper;
use app\models\UsersProfiles;
use yii\helpers\Url;
use yii\web\ConflictHttpException;
use kartik\form\ActiveForm;

class UsersProfilesController extends \yii\web\Controller
{
    public function actionIndex()
    {

        $user = UserHelper::getAuthUser();

        if(!$user){
            throw new ConflictHttpException('ERROR: access denied');
        }

        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => \Yii::t('Cm', 'Profile')
        ];

        $profile =  $user -> profile;
        $profile -> setScenario(UsersProfiles::SCENARIO_USER_EDIT);

        if (\Yii::$app->request->isAjax && $profile->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($profile);
        }

        if(\Yii::$app->request->isPost  && $profile->load(Yii::$app->request->post())) {
            $profile -> save();
            \Yii::$app->getSession()->setFlash('profile_saved', true);
            return $this->redirect(Url::toRoute('/user/profile'));
        }


       return $this->render('index', ['model' => $user -> profile]);
    }

}
