<?php

namespace app\controllers;

use app\components\helpers\NotificationHelper;
use app\components\helpers\UserHelper;
use app\models\News;
use app\models\Users;
use kartik\form\ActiveForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\ConflictHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\Pagination;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];

    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {

        $invalid_login = false;

        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => \Yii::t('Cm', 'Enter')
        ];

        $model = new Users(['scenario' => Users::SCENARIO_USER_LOGIN ]);

        if (\Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $login = UserHelper::login($model->login, $model->password);

            if ($login){
                return $this->goHome();
            }

            $invalid_login = true;
        }

        return $this->render('rar/login', [
            'model' => $model,
            'invalid_login' => $invalid_login
        ]);

 }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        UserHelper::LogOut();

        return $this->goHome();
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionRegistration()
    {

        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => \Yii::t('Cm', 'Registration')
        ];

        $model = new Users(['scenario' => Users::SCENARIO_USER_REGISTRATION]);


        if (\Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if(\Yii::$app->request->isPost  && $model->load(Yii::$app->request->post())) {

            $user_id = UserHelper::createUser($model -> login, $model -> password);

            if (!$user_id){
                throw new ConflictHttpException('ERROR: User dont created');
            }

            NotificationHelper::sendAfterRegistration($user_id, $model->password);
            \Yii::$app->getSession()->setFlash('account_must_activate', true);
            return $this->redirect(Url::toRoute('/registration'));
        }

        return $this->render('rar/registration', [
            'model' => $model
        ]);
    }


    public function actionActivation()
    {

        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => \Yii::t('Cm', 'Activation')
        ];

        $model = new Users(['scenario' => Users::SCENARIO_USER_ACTIVATION_ACCOUNT_HASH]);

        $hash = \Yii::$app->request->get('hash');
        if ($hash){
            $model -> hash = $hash;
        }

        $user = UserHelper::getByHash($model->hash);
        if (!$user){
            throw new ConflictHttpException('ERROR: Hash not find');
        }


        if (!UserHelper::activateAccount($user)){
            throw new ConflictHttpException('ERROR: Account not activited');
        };

        UserHelper::login($user -> login);
        NotificationHelper::sendAfterActivationAccount();
        return $this->goHome();

    }

    public function actionRestorePassword(){

        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => \Yii::t('Cm', 'Restore password')
        ];

        $model = new Users(['scenario' =>  Users::SCENARIO_USER_RESTORE_PASSWORD]);


        if (\Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if(\Yii::$app->request->isPost  && $model->load(Yii::$app->request->post())) {

            $user = UserHelper::restorePasswordGenerateHash($model -> login);

            if(!$user -> hash){
                throw new ConflictHttpException('Hash not generate');
            }

            NotificationHelper::sendAfterRestorePassword($user);
            \Yii::$app->session->setFlash('generated_hash');
            return $this->redirect(Url::toRoute('/restore-password'));
        }

        return $this->render('rar/restore_password', [
            'model' => $model
        ]);
    }


    public function actionNewPassword(){

        \Yii::$app->view->params['breadcrumbs'][] = [
            'label' => \Yii::t('Cm', 'New password')
        ];


        $hash = \Yii::$app->request -> get('hash');

        $user = UserHelper::getByHash($hash);

        if (!$hash || !$user){
            throw new ConflictHttpException('Hash is invalid');
        }

        $model = new Users(['scenario' =>  Users::SCENARIO_USER_RESTORE_NEW_PASSWORD]);

        if (\Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if(\Yii::$app->request->isPost  && $model->load(Yii::$app->request->post())) {

            UserHelper::setNewPassword($user, $model -> password);

            UserHelper::login($user -> login);
            NotificationHelper::sendAfterNewPassword($user, $model -> password);
            return $this->goHome();
        }

        return $this->render('rar/new_password', [
            'model' => $model
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }


    public function actionSendPrivate(){

        NotificationHelper::pusherSendTestMsg('private_channel', 'My private message');
        die('sendPrivate');
    }

    public function actionSendGroup(){

        NotificationHelper::pusherSendTestMsg('group_channel', 'My Group message');
        die('actionSendGroup');
    }

    public function actionSendAdmins(){

        NotificationHelper::pusherSendTestMsg('admins_channel', 'To admins message');
        die('actionSendAdmins');
    }

    public function actionSendGuest(){

       NotificationHelper::pusherSendTestMsg('guest_channel', 'To guest message');
        die('actionSendGuest');

    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionTest()
    {
        return $this->render('test');
    }



}
