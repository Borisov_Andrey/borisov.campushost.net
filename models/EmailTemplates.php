<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email_templates".
 *
 * @property integer $id
 * @property string $event
 * @property string $templates
 * @property string $created_at
 * @property string $updated_at
 */
class EmailTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['templates'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['event'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('Cm', 'ID'),
            'event' => Yii::t('Cm', 'Event'),
            'templates' => Yii::t('Cm', 'Templates'),
            'created_at' => Yii::t('Cm', 'Created At'),
            'updated_at' => Yii::t('Cm', 'Updated At'),
        ];
    }
}
