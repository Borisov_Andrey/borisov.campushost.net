<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_config".
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property string $created_at
 * @property string $updated_at
 */
class NewsConfig extends \yii\db\ActiveRecord
{
    const DEFAULT_COUNT_RECORDS_ON_PAGE = 10;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'value'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('Cm', 'ID'),
            'name' => Yii::t('Cm', 'Name'),
            'value' => Yii::t('Cm', 'Value'),
            'created_at' => Yii::t('Cm', 'Created At'),
            'updated_at' => Yii::t('Cm', 'Updated At'),
        ];
    }
}
