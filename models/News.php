<?php

namespace app\models;

use app\components\behaviors\MyBehavior;
use app\components\helpers\NotificationHelper;
use Yii;

use app\components\Helpers\YiiFormHtmlHelper;
use yii\helpers\Html;

use \kartik\builder\Form;


/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $preview
 * @property string $content
 * @property string $created_at
 * @property string $updated_at
 */
class News extends \yii\db\ActiveRecord
{

    const SCENARIO_EDIT = 'edit';
    const SCENARIO_ADD = 'ADD';


    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'app\components\behaviors\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url'
            ]
        ];
    }




    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['preview', 'content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'url'], 'string', 'max' => 255],

            [['name', 'preview', 'content'], 'required', 'on' => self::SCENARIO_EDIT],
            [['name', 'preview', 'content'], 'required', 'on' => self::SCENARIO_ADD],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('Cm', 'ID'),
            'name' => Yii::t('Cm', 'Name'),
            'url' => Yii::t('Cm', 'Url'),
            'preview' => Yii::t('Cm', 'Preview'),
            'content' => Yii::t('Cm', 'Content'),
            'created_at' => Yii::t('Cm', 'Created At'),
            'updated_at' => Yii::t('Cm', 'Updated At'),
        ];
    }

    public function getFormAttribs() {
        /********************************************************************/
        if($this->scenario == self::SCENARIO_EDIT || $this->scenario == self::SCENARIO_ADD){
            return [
                'name'              =>  YiiFormHtmlHelper::buildField(Form::INPUT_TEXT, false, \Yii::t('Cm', 'Enter name').'...',  \Yii::t('Cm', 'Example hit'), 'bullhorn'),
                'preview'           =>  YiiFormHtmlHelper::buildField(Form::INPUT_TEXTAREA, false, \Yii::t('Cm', 'Enter preview').'...',  \Yii::t('Cm', 'Example hit'), 'eye-open'),
                'content'           =>  YiiFormHtmlHelper::buildField(Form::INPUT_TEXTAREA, false, \Yii::t('Cm', 'Enter content').'...',  \Yii::t('Cm', 'Example hit'), 'eye-close'),
                'actions'           =>  ['type'=>Form::INPUT_RAW, 'value'=>Html::submitButton(\Yii::t('Cm', 'Save'), ['class'=>'btn btn-primary'])]
            ];
        }
    }

    public function afterSave($insert, $changedAttributes)
    {

        if ($insert) {
            NotificationHelper::sendAfterAddNews($this);
        } else {

        }


        parent::afterSave($insert, $changedAttributes);
    }

    //sendAfterAddNews
}
