<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_settings".
 *
 * @property integer $id
 * @property integer $role_id
 * @property integer $list
 * @property integer $view
 * @property integer $edit
 * @property integer $add
 * @property string $created_at
 * @property string $updated_at
 *
 * @property UsersRoles $role
 */
class NewsSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id'], 'required'],
            [['role_id', 'list', 'view', 'edit', 'add'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['role_id'], 'unique'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersRoles::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('Cm', 'ID'),
            'role_id' => Yii::t('Cm', 'Role ID'),
            'list' => Yii::t('Cm', 'List'),
            'view' => Yii::t('Cm', 'View'),
            'edit' => Yii::t('Cm', 'Edit'),
            'add' => Yii::t('Cm', 'Add'),
            'created_at' => Yii::t('Cm', 'Created At'),
            'updated_at' => Yii::t('Cm', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(UsersRoles::className(), ['id' => 'role_id']);
    }
}
