<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\Helpers\YiiFormHtmlHelper;
use yii\helpers\Html;
use yii\base\Model;
use \kartik\builder\Form;
use \kartik\form\ActiveField;

/**
 * This is the model class for table "users_profiles".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property integer $email_notification
 * @property integer $push_notification
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Users $user
 */
class UsersProfiles extends \yii\db\ActiveRecord
{
    const SCENARIO_USER_EDIT = 'user_edit';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'email_notification', 'push_notification'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['first_name', 'first_name'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],

            [['first_name', 'last_name'], 'required', 'on' => self::SCENARIO_USER_EDIT],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('Cm', 'ID'),
            'user_id' => Yii::t('Cm', 'User ID'),
            'first_name' => Yii::t('Cm', 'First Name'),
            'last_name' => Yii::t('Cm', 'Last Name'),
            'email_notification' => Yii::t('Cm', 'Email Notification'),
            'push_notification' => Yii::t('Cm', 'Push Notification'),
            'created_at' => Yii::t('Cm', 'Created At'),
            'updated_at' => Yii::t('Cm', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id'])->inverseOf('usersProfiles');
    }

    public function getFormAttribs() {
        /********************************************************************/
        if($this->scenario == self::SCENARIO_USER_EDIT){
            return [
                'first_name'            =>  YiiFormHtmlHelper::buildField(Form::INPUT_TEXT, false, \Yii::t('Cm', 'Enter first name').'...',  \Yii::t('Cm', 'Enter first name'), 'user'),
                'last_name'             =>  YiiFormHtmlHelper::buildField(Form::INPUT_TEXT, false, \Yii::t('Cm', 'Enter last name').'...',  \Yii::t('Cm', 'Enter last name'), 'user'),
                'email_notification'    =>  YiiFormHtmlHelper::buildField(Form::INPUT_CHECKBOX, false, \Yii::t('Cm', 'Enter password').'...',  \Yii::t('Cm', 'Example hit'), 'send'),
                'push_notification'     =>  YiiFormHtmlHelper::buildField(Form::INPUT_CHECKBOX, false, \Yii::t('Cm', 'Enter password').'...',  \Yii::t('Cm', 'Example hit'), 'send'),
                'actions'               =>  ['type'=>Form::INPUT_RAW, 'value'=>Html::submitButton(\Yii::t('Cm', 'Save'), ['class'=>'btn btn-primary'])]
            ];
        }
    }
}
