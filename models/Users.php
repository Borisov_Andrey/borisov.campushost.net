<?php

namespace app\models;

use app\components\helpers\NotificationHelper;
use Yii;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use app\components\Helpers\YiiFormHtmlHelper;
use yii\helpers\Html;
use yii\base\Model;
use \kartik\builder\Form;
use \kartik\form\ActiveField;



/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property integer $role_id
 * @property integer $account_status_id
 * @property string $hash
 * @property string $last_active_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AccountStatuses $accountStatus
 * @property UsersRoles $role
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{

    const ROLE_ADMIN = 1;
    const ROLE_EDITOR = 2;
    const ROLE_USER = 3;
    const ROLE_GUEST = 4;


    public $remember_me = true;
    public $password_repeat;

    const SCENARIO_ADMIN_CREATE_USER = 'admin_create_user';

    const SCENARIO_USER_REGISTRATION = 'user_registration';

    const SCENARIO_USER_LOGIN = 'user_login';

    const SCENARIO_USER_RESTORE_REGISTRATION = 'user_restore_registration';

    const SCENARIO_USER_ACTIVATION_ACCOUNT_HASH = 'user_activation_account_hash';

    const SCENARIO_USER_RESTORE_REGISTRATION_HASH = 'user_restore_registration_hash';

    const SCENARIO_USER_RESTORE_PASSWORD = 'user_restore_password';

    const SCENARIO_USER_RESTORE_NEW_PASSWORD = 'user_restore_new_password';

  //  const SCENARIO_USER_RESTORE_PASSWORD_HASH_KNOW = 'user_restore_password_hash_known';

    private $authKey;
    public $username;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id', 'account_status_id'], 'required'],
            [['role_id', 'account_status_id'], 'integer'],
            [['last_active_at', 'created_at', 'updated_at'], 'safe'],
            [['login', 'password', 'hash'], 'string', 'max' => 255],
            [['account_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => AccountStatuses::className(), 'targetAttribute' => ['account_status_id' => 'id']],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersRoles::className(), 'targetAttribute' => ['role_id' => 'id']],

            [['password'], 'string', 'min' => 6, 'max' => 255],

            [['login'], 'string', 'min' => 6, 'max' => 255],
            [['login'], 'email'],
            [['login'], 'unique', 'on' => self::SCENARIO_USER_REGISTRATION],

            [['login'], 'emailAlreadyExistOnRestorePassword', 'on' => self::SCENARIO_USER_RESTORE_PASSWORD],
            [['login'], 'required', 'on' => self::SCENARIO_USER_RESTORE_PASSWORD],


            [['login', 'password'], 'required', 'on' => self::SCENARIO_USER_LOGIN],

            [['password', 'password_repeat'], 'required', 'on' => self::SCENARIO_USER_REGISTRATION],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'skipOnEmpty' => false, 'message'=>\Yii::t('Cm', "Passwords don't match"), 'on' => self::SCENARIO_USER_REGISTRATION],

            [['hash'], 'required', 'on' => self::SCENARIO_USER_ACTIVATION_ACCOUNT_HASH],
            [['hash'], 'hashAlreadyExistOnActivationAccountHash', 'on' => self::SCENARIO_USER_ACTIVATION_ACCOUNT_HASH],


            [['password', 'password_repeat'], 'required', 'on' => self::SCENARIO_USER_RESTORE_NEW_PASSWORD],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'skipOnEmpty' => false, 'message'=>\Yii::t('Cm', "Passwords don't match"), 'on' => self::SCENARIO_USER_RESTORE_NEW_PASSWORD],


            [['login'], 'unique', 'on' => self::SCENARIO_ADMIN_CREATE_USER],
            [['login', 'password', 'account_status_id', 'role_id'], 'required', 'on' => self::SCENARIO_ADMIN_CREATE_USER],
        ];
    }

    public function emailAlreadyExistOnRestorePassword($attribute, $params){
        return (!self::find()->where(['login' => $this -> login])->exists()) ? $this->addError($attribute, \Yii::t('Cm', 'Email not find')) : true;
    }

    public function hashAlreadyExistOnActivationAccountHash($attribute, $params){
        return (!self::find()->where(['hash' => $this -> hash])->exists()) ? $this->addError($attribute, \Yii::t('Cm', 'Hash not find')) : true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('Cm', 'ID'),
            'login' => Yii::t('Cm', 'Login'),
            'password' => Yii::t('Cm', 'Password'),
            'role_id' => Yii::t('Cm', 'Role ID'),
            'account_status_id' => Yii::t('Cm', 'Account Status ID'),
            'hash' => Yii::t('Cm', 'Hash'),
            'last_active_at' => Yii::t('Cm', 'Last Active At'),
            'created_at' => Yii::t('Cm', 'Created At'),
            'updated_at' => Yii::t('Cm', 'Updated At'),

            'remember_me'            => Yii::t('Cm', 'Remember Me'),
            'password_repeat'       => Yii::t('Cm', 'Repeat password'),
        ];
    }

    public function getFormAttribs() {
        /********************************************************************/
        if($this->scenario == self::SCENARIO_USER_LOGIN){
            return [
                'login'             =>  YiiFormHtmlHelper::buildField(Form::INPUT_TEXT, false, \Yii::t('Cm', 'Enter email').'...',  \Yii::t('Cm', 'Enter email'), 'envelope'),
                'password'          =>  YiiFormHtmlHelper::buildField(Form::INPUT_PASSWORD, false, \Yii::t('Cm', 'Enter password').'...',  \Yii::t('Cm', 'Do not share your password'), 'eye-close'),
                'remember_me'       =>  ['type'=>Form::INPUT_CHECKBOX],
                'actions'           =>  ['type'=>Form::INPUT_RAW, 'value'=>Html::submitButton(\Yii::t('Cm', 'Enter'), ['class'=>'btn btn-primary'])]
            ];
        }


        /********************************************************************/
        if($this->scenario == self::SCENARIO_USER_REGISTRATION){
            return [
                'login'             =>  YiiFormHtmlHelper::buildField(Form::INPUT_TEXT, false, \Yii::t('Cm', 'Enter email').'...',  \Yii::t('Cm', 'Enter email'), 'envelope'),
                'password'          =>  YiiFormHtmlHelper::buildField(Form::INPUT_PASSWORD, false, \Yii::t('Cm', 'Enter password').'...',  \Yii::t('Cm', 'Do not share your password'), 'eye-close'),
                'password_repeat'   =>  YiiFormHtmlHelper::buildField(Form::INPUT_PASSWORD,         false,                                              \Yii::t('Cm', 'Repeat password').'...',         \Yii::t('Cm', 'Repeat password'), 'eye-close'),
                'actions'           =>  ['type'=>Form::INPUT_RAW, 'value'=>Html::submitButton(\Yii::t('Cm', 'Submit'), ['class'=>'btn btn-primary'])]
            ];
        }

        /********************************************************************/
        if($this->scenario == self::SCENARIO_USER_RESTORE_PASSWORD){
            return [
                'login'             =>  YiiFormHtmlHelper::buildField(Form::INPUT_TEXT, false, \Yii::t('Cm','Enter email').'...',  \Yii::t('Cm','Enter email').'...', 'envelope'),
                'actions'           =>  ['type'=>Form::INPUT_RAW, 'value'=>Html::submitButton(\Yii::t('Cm', 'Submit'), ['class'=>'btn btn-primary'])]
            ];
        }

        /********************************************************************/
        if($this->scenario == self::SCENARIO_USER_ACTIVATION_ACCOUNT_HASH){
            return [
                'hash'              =>  YiiFormHtmlHelper::buildField(Form::INPUT_TEXT, false, \Yii::t('Cm','Enter hash').'...',   \Yii::t('Cm','Enter hash').'...', 'barcode'),
                'actions'           =>  ['type'=>Form::INPUT_RAW, 'value'=>Html::submitButton(\Yii::t('Cm', 'Submit'), ['class'=>'btn btn-primary'])]
            ];
        }

        /********************************************************************/
        if($this->scenario == self::SCENARIO_USER_RESTORE_NEW_PASSWORD){
            return [
                'password'          =>  YiiFormHtmlHelper::buildField(Form::INPUT_PASSWORD, false, \Yii::t('Cm', 'Enter password').'...',  \Yii::t('Cm', 'Do not share your password'), 'eye-close'),
                'password_repeat'   =>  YiiFormHtmlHelper::buildField(Form::INPUT_PASSWORD,         false,                                              \Yii::t('Cm', 'Repeat password').'...',         \Yii::t('Cm', 'Repeat password'), 'eye-close'),
                'actions'           =>  ['type'=>Form::INPUT_RAW, 'value'=>Html::submitButton(\Yii::t('Cm', 'Submit'), ['class'=>'btn btn-primary'])]
            ];
        }

    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountStatus()
    {
        return $this->hasOne(AccountStatuses::className(), ['id' => 'account_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(UsersProfiles::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(UsersRoles::className(), ['id' => 'role_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if(self::findIdentity($this -> id)['password'] !=  $this -> password ) {
                $this->password = self::generatePassword($this->password);
            }

            if ($this->isNewRecord) {

            }

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $user = Users::find()->where(['id' => $id])->asArray()->one();
        return !empty($user) ? new static($user) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = User::find()->where(['access_token' => $token])->asArray()->one();
        return !empty($user) ? new static($user) : null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user = Users::find()->where(['login' => $username])->asArray()->one();
        return !empty($user) ? new static($user) : null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($hash)
    {
        return Yii::$app->getSecurity()->validatePassword($hash, $this->password);
    }

    public static function generatePassword($password){
        return Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public static function findByLogin($login){
        return static::findOne(['login' => $login, 'account_status_id' => 1]);
    }
}
