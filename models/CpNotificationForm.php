<?php

namespace app\models;

use Yii;
use yii\base\Model;

class CpNotificationForm extends Model
{
    public $users;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['users'], 'safe'],
        ];
    }
}
