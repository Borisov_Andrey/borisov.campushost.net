Версия для тестирования http://borisov.campushost.net/
ПМА http://shared.campushost.net/myadmin/     username:borisov_b-test, password: b-test
Репозиторий https://bitbucket.org/Borisov_Andrey/borisov.campushost.net
Дамп БД в корне dump.sql

Емайл ящики для тестирования
(использован сервис рассылки mailgun.com, у меня нет возможности на шаред хостинке верифицировань домен и DNS, потому есть ограничения по доступным емайлам)

Логин/пароль на mail.ru
borisov_test_user@mail.ru/434198599446685c85a5fc14f12befd1
borisov_test_user2@mail.ru/ebe9cd18aa40ba89fefdf9e13c5a36fc
borisov_test_editor@mail.ru/10b02e14abe2580054332ba3938df6fb
borisov_test_adm@mail.ru/9d5f9276e4cbdafe3d48b540722f5f40

Логин/пароль на http://borisov.campushost.net/
borisov_test_user@mail.ru/borisov_test_user@mail.ru
borisov_test_user2@mail.ru/borisov_test_user2@mail.ru
borisov_test_editor@mail.ru/borisov_test_editor@mail.ru
borisov_test_adm@mail.ru/borisov_test_adm@mail.ru - администратор, с доступом в панель управления,
ссылка на вход будет видна после авторизации http://joxi.ru/Y2L0XjNIny8va2,
или http://borisov.campushost.net/login


Коротко о главном.
Использованы расширения
1) kartik-v/yii2-builder, с помощью которого управления простыми формами переслось в модель, пример app\models\Users через выбор сценария, и описанными полями в getFormAttribs(),
    далее используется один шаблон для всех подобных вариантов.
    Это дает отойти от заботы управлением формами в шаблонах, быстро и комфортно получить типичную форму.
2) для нофификаций (всплывающих окон) использован пушер pusher.com с внешним рашрирением стилей всплывающих окно.
    Pusher дает возможность управлять таким типом сообщений на мобильных платформах.
3) для емайл рассылки испьльзован mailgun.com. Он может дать возможность пакетной рассылки писем до 1к писем за запрос.
4) испльзованы миграции
5) transliterator-helper для создания slug-ов для новостей
6) kartik-v/yii2-widget-select2 для селектов и мультиселектов в админке (примеры http://joxi.ru/n2Y5k86tjXRw62 http://joxi.ru/DmBDNX3SN9jvyA)
7) круды в админке в основном на базе штатных Yii2
8) использован механизм переводом(мультиязычности)
9) использован механизм поведений на примере slug для новостей \components\behaviors\Slug.php в \models\News.php->behaviors(),
    также прослушивание событий модели, к примеру при создании новости \models\News.php->afterSave()
10) использованны триггреры MySql для установки времени создания и обновления таблиц БД

Время: около 40ч
Ссылка на резюме https://hh.ru/resume/04d0a5f4ff03656da10039ed1f763552474539


